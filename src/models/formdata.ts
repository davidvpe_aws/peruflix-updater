export type FormDataFile = FormDataFileDownloaded | FormDataFileNotDownloaded

export type FormDataFileDownloaded = {
  downloaded: true
  type: 'path'
  path: string
  filename: string
}

type FormDataFileNotDownloaded = {
  downloaded: false
  type: 'path'
  value: Buffer
  filename: string
}

export type ParsedFormData = {
  fields: Record<string, unknown>
  files: Record<string, FormDataFile>
}
