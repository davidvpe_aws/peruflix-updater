import * as D from 'io-ts/Decoder'
import { pipe } from 'fp-ts/function'
import { FormDataFile, FormDataFileDownloaded } from './formdata'

export const eventDecoder = D.struct({
  body: D.string,
  httpMethod: D.string,
  headers: pipe(
    D.record(D.string),
    D.intersect(
      D.struct({
        'content-type': D.string
      })
    )
  )
})

export type ApiGatewayEvent = D.TypeOf<typeof eventDecoder>

export const plexEventDecoder = D.struct({
  event: D.string,
  Account: D.struct({
    thumb: D.string,
    title: D.string
  })
})

export type DecodedPlexEvent = D.TypeOf<typeof plexEventDecoder>

export type DecodedPlexEventWithFiles = DecodedPlexEvent & {
  files: FormDataFile[]
}

export type DecodedPlexEventWithFilesDownloaded = DecodedPlexEvent & {
  files: FormDataFileDownloaded[]
}
