import { flow, pipe } from 'fp-ts/lib/function'
import * as TE from 'fp-ts/TaskEither'
import * as A from 'fp-ts/Array'
import { FormDataFile, FormDataFileDownloaded } from '../models/formdata'
import fs from 'fs/promises'
import fsSync from 'fs'
import { DecodedPlexEventWithFiles, DecodedPlexEventWithFilesDownloaded } from '../models/event'

const tmpFolder = '/tmp'

const downloadFile = (file: FormDataFile): TE.TaskEither<Error, FormDataFileDownloaded> =>
  pipe(
    file.downloaded
      ? TE.right(file)
      : pipe(
          TE.tryCatch<Error, void>(
            async () => {
              const writeStream = fsSync.createWriteStream(`/tmp/${file.filename}`)
              writeStream.write(file.value)
              writeStream.end()
              return
            },
            (e) => (e instanceof Error ? e : new Error('Error writing file'))
          ),
          TE.map<void, FormDataFileDownloaded>(() => ({
            filename: file.filename,
            downloaded: true,
            path: `/tmp/${file.filename}`,
            type: 'path'
          }))
        )
  )

export const saveInnerFiles = (
  formData: DecodedPlexEventWithFiles
): TE.TaskEither<Error, DecodedPlexEventWithFilesDownloaded> =>
  pipe(
    formData.files,
    A.traverse(TE.ApplicativeSeq)((file) => downloadFile(file)),
    TE.map((downloadedFiles) => ({ ...formData, files: downloadedFiles }))
  )
