import * as TE from 'fp-ts/TaskEither'
import * as O from 'fp-ts/Option'
import { pipe } from 'fp-ts/lib/function'
import { ApiGatewayEvent } from '../models/event'
import { FormDataFile, ParsedFormData } from '../models/formdata'

const parseBody =
  (body: string) =>
  (boundary: string): TE.TaskEither<Error, ParsedFormData> => {
    const parts = body.split(`--${boundary}`)
    const fields: Record<string, unknown> = {}
    const files: Record<string, FormDataFile> = {}

    for (const part of parts) {
      const [header, body] = part.trim().split('\r\n\r\n')
      if (!header || !body) {
        continue
      }

      const headers = header.split('\r\n')
      const dispositionHeader = headers.find((h) => h.startsWith('Content-Disposition:'))
      const contentTypeHeader = headers.find((h) => h.startsWith('Content-Type:'))

      if (!dispositionHeader) {
        continue
      }

      const dispositionMatch = dispositionHeader.match(/name="([^"]+)"/)
      if (!dispositionMatch) {
        continue
      }

      try {
        const name = dispositionMatch[1]
        if (name === 'payload' && contentTypeHeader && contentTypeHeader.includes('application/json')) {
          Object.entries(JSON.parse(body)).forEach(([key, value]) => {
            fields[key] = value
          })
        } else if (contentTypeHeader) {
          const contentTypeMatch = contentTypeHeader.match(/Content-Type: (.+)/)
          const fileType = contentTypeMatch ? contentTypeMatch[1] : ''
          const extension = fileType.split('/').pop() || ''
          files[name] = {
            downloaded: false,
            type: 'path',
            value: Buffer.from(body, 'base64'), //could be binary
            filename: `${name}.${extension}`
          }

          console.log('fileSize', body.length, 'bytes')
          console.log('body', body)
        } else {
          fields[name] = JSON.parse(body)
        }
      } catch (err) {
        return TE.left(new Error(`Error parsing form data: ${String(err)}`))
      }
    }
    return TE.right({ fields, files })
  }

export function parseFormData(event: ApiGatewayEvent): TE.TaskEither<Error, ParsedFormData> {
  return pipe(
    event.headers['content-type'].match(/boundary=(?:"([^"]+)"|([^;]+))/i),
    O.fromNullable,
    O.chain((m) => O.fromNullable(m[1] || m[2])),
    O.fold(
      () => TE.left(new Error('Could not parse boundary')),
      (boundary) => TE.right(boundary)
    ),
    TE.chain(parseBody(event.body))
  )
}
