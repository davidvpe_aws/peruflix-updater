import { flow, pipe } from 'fp-ts/lib/function'
import * as D from 'io-ts/Decoder'
import * as E from 'fp-ts/Either'
import * as A from 'fp-ts/Array'
import * as TE from 'fp-ts/TaskEither'
import * as T from 'fp-ts/Task'
import * as C from 'fp-ts/Console'

import axios, { AxiosError } from 'axios'

const eventDecoder = D.struct({
  body: D.string
})

const seriesUpdateDecoder = D.struct({
  series: D.struct({
    id: D.number,
    title: D.string,
    path: D.string,
    tvdbId: pipe(D.number, D.nullable),
    imdbId: pipe(D.string, D.nullable)
  }),
  episodes: D.array(
    D.struct({
      id: D.number,
      episodeNumber: D.number,
      seasonNumber: D.number,
      title: D.string
    })
  ),
  episodeFile: D.struct({
    id: D.number,
    quality: D.string
  })
})

const movieUpdateDecoder = D.struct({
  movie: D.struct({
    id: D.number,
    title: D.string,
    year: D.number,
    tmdbId: pipe(D.number, D.nullable),
    imdbId: pipe(D.string, D.nullable)
  }),
  movieFile: D.struct({
    id: D.number,
    quality: D.string
  })
})

const updateDecoder = D.union(seriesUpdateDecoder, movieUpdateDecoder)

type SeriesUpdate = D.TypeOf<typeof seriesUpdateDecoder>
type MovieUpdate = D.TypeOf<typeof movieUpdateDecoder>
type UnkownUpdate = D.TypeOf<typeof updateDecoder>

const parseEvent = flow(
  eventDecoder.decode,
  E.chain(
    flow(
      ({ body }) =>
        E.tryCatch(
          () => JSON.parse(body),
          (e) => D.error(body, `JSON. OriginalError: ${e instanceof Error ? e.message : "Couldn't parse JSON"}`)
        ),
      E.chain(updateDecoder.decode)
    )
  ),
  E.mapLeft(flow(D.draw, (s) => new Error(s)))
)

type TelegramUpdate = {
  type: 'Movie' | 'Series'
  message: string
}

const processReservedCharacters = (message: string) => message.replace(/([_*[\]()~`>#+-=|{}.!])/g, '\\$1')

const generateTelegramMessage = (update: TelegramUpdate) => {
  let message = ''
  switch (update.type) {
    case 'Movie':
      message = `🎬 *Movie Downloaded* 🎬`
      break
    case 'Series':
      message = `📺 *Episode Downloaded* 📺`
      break
  }

  return `${message}\n\n${update.message}`
}

const processSeries = (update: SeriesUpdate): E.Either<Error, TelegramUpdate> =>
  pipe(
    E.Do,
    E.bind('file', () => E.right(update.episodeFile)),
    E.bind('series', () => E.right(update.series)),
    E.bind('firstEpisode', () =>
      pipe(
        A.head(update.episodes),
        E.fromOption(() => new Error('No episodes found'))
      )
    ),
    E.bindW('episodeCode', ({ firstEpisode }) =>
      pipe(
        `S${firstEpisode.seasonNumber.toString().padStart(2, '0')}E${firstEpisode.episodeNumber
          .toString()
          .padStart(2, '0')}`,
        E.right
      )
    ),
    E.bind('url', ({ series }) => {
      if (series.imdbId) {
        return E.right(`https://www.imdb.com/title/${series.imdbId}`)
      } else if (series.tvdbId) {
        return E.right(`https://thetvdb.com/?tab=series&id=${series.tvdbId}`)
      } else {
        return E.left(new Error("Couldn't find a nor imdbId or tvdbId for the series"))
      }
    }),
    E.map(({ series, firstEpisode, episodeCode, url, file }) =>
      [`${series.title} - ${episodeCode} - ${firstEpisode.title} [${file.quality}]`, url].join('\n')
    ),
    E.map(processReservedCharacters),
    E.map((message) => ({ type: 'Series', message }))
  )

const processMovie = (update: MovieUpdate): E.Either<Error, TelegramUpdate> =>
  pipe(
    E.Do,
    E.bind('movie', () => E.right(update.movie)),
    E.bind('quality', () => E.right(update.movieFile.quality)),
    E.bind('url', ({ movie }) => {
      if (movie.imdbId) {
        return E.right(`https://www.imdb.com/title/${movie.imdbId}`)
      } else if (movie.tmdbId) {
        return E.right(`https://www.themoviedb.org/movie/${movie.tmdbId}`)
      } else {
        return E.left(new Error("Couldn't find a nor imdbId or tmdbId for the movie"))
      }
    }),
    E.map(({ movie, url, quality }) => [`${movie.title} (${movie.year}) [${quality}]`, url].join('\n')),
    E.map(processReservedCharacters),
    E.map((message) => ({ type: 'Movie', message }))
  )

const telegramURL = `https://api.telegram.org/bot${process.env.TELEGRAM_TOKEN}/sendMessage`

const chatId = process.env.TELEGRAM_CHAT_ID || process.env.TELEGRAM_UPDATER_CHAT_ID || ''

const sendTelegramMessage = (message: string) =>
  pipe(
    TE.tryCatch(
      () =>
        axios
          .post(telegramURL, { text: message, chat_id: chatId, parse_mode: 'MarkdownV2' })
          .then((r) => r.data)
          .then(JSON.stringify),
      flow(
        (e) => e as Error,
        (e) => (e instanceof AxiosError ? pipe(e.response?.data, (obj) => JSON.stringify(obj, null, 2)) : e.message),
        Error
      )
    )
  )

const generateUpdate = (update: UnkownUpdate): E.Either<Error, TelegramUpdate> => {
  if ('series' in update) {
    return processSeries(update)
  } else if ('movie' in update) {
    return processMovie(update)
  } else {
    return E.left(new Error('Unknown update type'))
  }
}

type TelegramUpdateResult = {
  statusCode: number
  body: string
}

export const main = flow(
  parseEvent,
  E.chainW(generateUpdate),
  E.map(generateTelegramMessage),
  TE.fromEither,
  TE.chain(sendTelegramMessage),
  TE.fold<Error, string, string>(
    flow((err) => err.message, T.of),
    T.of
  ),
  T.chainFirstIOK(C.log),
  T.map((body) => ({ statusCode: 200, body })),
  (t) => t()
)

export const handler = main
