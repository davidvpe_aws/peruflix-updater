import * as fsSync from "fs";
import * as fs from "fs/promises";
import axios, { AxiosError } from "axios";
import busboy from "busboy";

import Log from "@dazn/lambda-powertools-logger";

import * as D from "io-ts/Decoder";

import { flow, identity, pipe } from "fp-ts/function";
import * as O from "fp-ts/Option";
import * as A from "fp-ts/Array";
import * as T from "fp-ts/Task";
import * as C from "fp-ts/Console";
import * as IO from "fp-ts/IO";
import * as TE from "fp-ts/TaskEither";
import FormData from "form-data";
import {
  DecodedPlexEvent,
  DecodedPlexEventWithFiles,
  eventDecoder,
  plexEventDecoder,
} from "./models/event";
import { FormDataFileDownloaded } from "./models/formdata";
import { parseFormData } from "./utils/form-data-parser";
import { saveInnerFiles } from "./utils/file-downloader";

const botToken = process.env.TELEGRAM_TOKEN || "";
const chatId =
  process.env.TELEGRAM_CHAT_ID || process.env.TELEGRAM_NOTIFIER_CHAT_ID || "";

// const printWithParams = <A>(message: string) => (params: A) =>

const print =
  <A>(message: string, params?: A) =>
  (a: A) =>
    pipe(a, (a) => Log.info(message, { params: params || a }), IO.of);

const parse =
  <A>(decoder: D.Decoder<unknown, A>) =>
  (a: unknown) =>
    pipe(
      a,
      T.of,
      T.chainFirstIOK(print("Event")),
      TE.fromTask,
      TE.chain(flow(decoder.decode, TE.fromEither)),
      TE.mapLeft(flow(D.draw, Error))
    );

const jsonPrettier = (json: object) => JSON.stringify(json, null, 2);

const saveToFS =
  (path: string, type: TelegramFile["contentType"]) =>
  <T extends object>(body: T) =>
    pipe(
      TE.tryCatch(
        () => fs.writeFile(path, pipe(body, jsonPrettier)),
        (e) => (e instanceof Error ? e : new Error("File write error"))
      ),
      TE.chainFirstIOK(print("Saved to FS")),
      TE.map<void, TelegramFile & T>(() => ({
        value: path,
        type: "path",
        contentType: type,
        ...body,
      }))
    );

const convertToFormData = <A extends object>(path: A) => {
  const formData = new FormData();
  Object.entries(path).forEach(([key, value]) => {
    formData.append(key, value);
  });
  return formData;
};

type TelegramFile = {
  type: "path" | "url";
  value: string;
  contentType: "photo" | "document";
};

const getSendDocumentBody = (
  chatId: string,
  source: TelegramFile,
  message?: string
) =>
  pipe(
    source,
    (source) =>
      O.fromNullable(
        source.type === "path"
          ? fsSync.createReadStream(source.value)
          : source.type === "url"
          ? source.value
          : null
      ),
    O.map((document) => ({
      chat_id: chatId,
      ...(source.contentType === "photo"
        ? { photo: document }
        : source.contentType == "document"
        ? { document }
        : {}),
      ...(message ? { caption: message, parse_mode: "Markdown" } : {}),
    })),
    O.map(convertToFormData)
  );

const convertAxiosToError = (e: AxiosError) =>
  pipe(
    e,
    (e) => ({
      statusCode: e.response?.status,
      statusText: e.response?.statusText,
      headers: e.response?.headers,
      responseData: e.response?.data,
    }),
    JSON.stringify,
    (s) => new Error(`Axios Error: \n ${s}`)
  );

const sendFileToTelegram =
  <T extends TelegramFile>(
    chatId: string,
    botToken: string,
    message?: string
  ) =>
  (source: T) =>
    pipe(
      TE.Do,
      TE.bind("url", () => {
        switch (source.contentType) {
          case "photo":
            return TE.right(
              `https://api.telegram.org/bot${botToken}/sendPhoto`
            );
          case "document":
            return TE.right(
              `https://api.telegram.org/bot${botToken}/sendDocument`
            );
        }
      }),
      TE.chainFirstIOK(print(`Sending file to Telegram`)),
      TE.bind("body", () =>
        pipe(
          getSendDocumentBody(chatId, source, message),
          TE.fromOption(() => new Error("Body could not be created"))
        )
      ),
      TE.chainFirstIOK(({ body }) => pipe(Log.info("body", body), IO.of)),
      TE.chain(({ url, body }) =>
        TE.tryCatch(
          () =>
            axios.post(url, body, {
              headers: body.getHeaders(),
            }),
          (e) =>
            e instanceof AxiosError
              ? convertAxiosToError(e)
              : e instanceof Error
              ? e
              : new Error("Telegram API error")
        )
      ),
      TE.map((response) => response.data)
    );

const sendObjectToTelegram =
  <T extends object>(chatId: string, botToken: string, message?: string) =>
  (source: T) =>
    pipe(
      source,
      saveToFS("/tmp/event.log", "document"),
      TE.chainFirst(sendFileToTelegram(chatId, botToken, message)),
      TE.fold(
        flow((e) => e.message, C.error, T.fromIO),
        flow((r) => JSON.stringify(r, null, 2), C.log, T.fromIO)
      ),
      (t) => TE.fromTask<void, Error>(t)
    );

// const decodePlexEvent = identity

// Instead of doing two parses, do one parse with https://github.com/francismeynard/lambda-multipart-parser/blob/master/index.js

export const handler = flow(
  parse(eventDecoder),
  TE.chain(
    flow(
      parseFormData,
      TE.chain((parsedFormData) =>
        pipe(
          parsedFormData.fields,
          parse(plexEventDecoder),
          TE.map<DecodedPlexEvent, DecodedPlexEventWithFiles>(
            (parsedEvent) => ({
              ...parsedEvent,
              files: pipe(parsedFormData.files, (files) =>
                Object.values(files)
              ),
            })
          )
        )
      )
    )
  ),
  TE.chain(saveInnerFiles),
  TE.chainFirst(sendObjectToTelegram(chatId, botToken, "Event sent")),
  TE.chain((event) =>
    pipe(
      event.files,
      A.head,
      O.fold<FormDataFileDownloaded, TelegramFile>(
        () => ({
          type: "url",
          value: event.Account.thumb + "&size=256",
          contentType: "photo",
        }),
        (file) => ({
          type: "path",
          value: file.path,
          contentType: "photo",
        })
      ),
      sendFileToTelegram(chatId, botToken, event.event || "")
    )
  ),
  TE.fold(
    flow((e) => e.message, C.error, T.fromIO),
    flow((r) => JSON.stringify(r, null, 2), C.log, T.fromIO)
  ),
  T.map(() => ({ statusCode: 200, body: "OK" })),
  (t) => t()
);
