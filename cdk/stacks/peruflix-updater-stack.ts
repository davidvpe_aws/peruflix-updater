import * as cdk from "aws-cdk-lib";
import { LambdaIntegration, RestApi } from "aws-cdk-lib/aws-apigateway";
import { Runtime } from "aws-cdk-lib/aws-lambda";
import { NodejsFunction } from "aws-cdk-lib/aws-lambda-nodejs";
import { RetentionDays } from "aws-cdk-lib/aws-logs";
import { Construct } from "constructs";
// import * as sqs from 'aws-cdk-lib/aws-sqs';

type PeruflixUpdaterStackProps = cdk.StackProps & {
  resourcePrefix: string;
};

export class PeruflixUpdaterStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props: PeruflixUpdaterStackProps) {
    super(scope, id, props);

    const telegramBotToken = process.env.TELEGRAM_TOKEN || "";
    const updaterChatId = process.env.TELEGRAM_UPDATER_CHAT_ID || "";
    // const notifierChatId = process.env.TELEGRAM_NOTIFIER_CHAT_ID || ''

    const lambdaUpdater = new NodejsFunction(
      this,
      `${props.resourcePrefix}-updater`,
      {
        functionName: `${props.resourcePrefix}-updater`,
        entry: "src/updater.ts",
        runtime: Runtime.NODEJS_20_X,
        logRetention: RetentionDays.ONE_WEEK,
        environment: {
          TELEGRAM_TOKEN: telegramBotToken,
          TELEGRAM_CHAT_ID: updaterChatId,
        },
      }
    );

    const api = new RestApi(this, `${props.resourcePrefix}-updater-api`, {
      restApiName: `${props.resourcePrefix}-updater-api`,
    });

    api.root.addMethod("POST", new LambdaIntegration(lambdaUpdater));

    // const lambdaNotifier = new NodejsFunction(this, `${props.resourcePrefix}-notifier`, {
    //   functionName: `${props.resourcePrefix}-notifier`,
    //   entry: 'src/server-notifier.ts',
    //   runtime: Runtime.NODEJS_16_X,
    //   logRetention: RetentionDays.ONE_WEEK,
    //   environment: {
    //     TELEGRAM_TOKEN: telegramBotToken,
    //     TELEGRAM_CHAT_ID: notifierChatId
    //   }
    // })

    // const apiNotifier = new RestApi(this, `${props.resourcePrefix}-notifier-api`, {
    //   restApiName: `${props.resourcePrefix}-notifier-api`
    // })

    // apiNotifier.root.addMethod('POST', new LambdaIntegration(lambdaNotifier))
  }
}
