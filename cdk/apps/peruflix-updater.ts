#!/usr/bin/env node
import 'source-map-support/register'
import * as cdk from 'aws-cdk-lib'
import { PeruflixUpdaterStack } from '../stacks/peruflix-updater-stack'

const app = new cdk.App()
new PeruflixUpdaterStack(app, 'peruflix-updater-stack', {
  resourcePrefix: 'peruflix',
  env: {
    region: 'eu-central-1'
  }
})
